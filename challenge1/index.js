var request = require('request');
var editJsonFile = require('edit-json-file');
var sha1 = require('sha1');
var fs = require('fs');
var initialDataUrl = 'https://api.codenation.dev/v1/challenge/dev-ps/generate-data?token=3427957343ef110e313a1f49b0e8eb6a495bc7f5'
var destinyUrl = 'https://api.codenation.dev/v1/challenge/dev-ps/submit-solution?token=3427957343ef110e313a1f49b0e8eb6a495bc7f5';
var fileUrl = './answer'

function createFile(originalData) {
    fs.writeFile(fileUrl, originalData, 'utf8', function (err) {
        if (err) throw err;
        console.log('file saved!');
    });
}

function decrypt(text, factor) {
    var result = '';
    for (var i = 0; i < text.length; i++) {
        if (text.charAt(i) != '.' && text.charAt(i) != ' ') {

            var after = String.fromCharCode(text.charCodeAt(i) - factor);
            result = result + after;

        } else {
            result = result + text.charAt(i);
        }
    }
    console.log('original:' + text);
    console.log('result  :' + result);

    return result;
}

function sendResult() {
    const options = {
        method: "POST",
        url: destinyUrl,
        headers: {
            "Content-Type": "multipart/form-data"
        },
        formData: {
            "answer": fs.createReadStream(fileUrl)
        }
    };

    request(options, function (err, _res, body) {
        if (err) console.log(err);
        console.log(body);
    });
}

fs.exists(fileUrl, (exists) => {
    if (!exists) {
        request(initialDataUrl, function (error, _response, body) {
            if (error) throw err;
            createFile(body);
        });
    }
    else {
        var file = editJsonFile(fileUrl, { autosave: true });

        var decrypted = decrypt(file.get('cifrado').toLowerCase(), file.get('numero_casas'));
        var resume = sha1(decrypted).toLowerCase();
        console.log(resume);

        file.set("decifrado", decrypted.toLowerCase());
        file.set('resumo_criptografico', resume);

        sendResult();

    }
});
